package deloitte.academy.lesson01.entity;

public abstract class Cliente {
	private int id;
	private String nombre;
	private double precio;

	/**
	 * M�todo constructor de la clase abstracta de cliente
	 * @param nombre del cliente que ser� registrado
	 * @param id que ser� asignado al cliente
	 */
	public Cliente(String nombre, int id) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.precio = 0;
	}

	/*
	 * M�todos getters y setters
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public abstract double cobrar();
}
