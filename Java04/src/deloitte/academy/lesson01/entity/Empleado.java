package deloitte.academy.lesson01.entity;

public class Empleado extends Cliente {
	/*
	 * 
	 * M�todo constructor para un cliente de tipo empleado, hereda de la clase cliente
	 */
	public Empleado(String nombre, int id) {
		super(nombre, id);
		// TODO Auto-generated constructor stub
	}

	/*
	 * M�todo de cobro sobre escrito en el cual se cobra s�lo el 85% de la tarifa
	 */
	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		return this.getPrecio() * 0.85;
	}
}
