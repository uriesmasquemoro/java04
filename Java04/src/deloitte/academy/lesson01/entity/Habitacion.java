package deloitte.academy.lesson01.entity;

import java.util.ArrayList;

public class Habitacion {
	private int numero;
	private Cliente cliente;
	private boolean status;

	public static ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

	/**
	 * Constructor de la clase habitaci�n, s�lo es requerido indicar su n�mero para
	 * que sea creada
	 * 
	 * @param numero
	 */
	public Habitacion(int numero) {
		super();
		this.numero = numero;
		this.cliente = null;
		this.status = true;
		habitaciones.add(this);
	}

	/*
	 * 
	 * M�todos de getters y setters
	 */
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean getStatus() {
		return this.status;
	}
}
