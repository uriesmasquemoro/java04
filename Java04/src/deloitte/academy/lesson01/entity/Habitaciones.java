package deloitte.academy.lesson01.entity;

public enum Habitaciones {
	BASIC(1500),
	PRO(1500),
	PREMIUM(6000);

	public int costo;

	/*
	 * Constructor del enum de habitaciones
	 */
	Habitaciones(int costo) {
		// TODO Auto-generated constructor stub
		this.costo = costo;
	}

	public int getCosto() {
		return costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}
}
