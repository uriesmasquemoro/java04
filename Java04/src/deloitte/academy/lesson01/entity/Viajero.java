package deloitte.academy.lesson01.entity;

public class Viajero extends Cliente {
	/*
	 * 
	 * M�todo constructor para un cliente de tipo viajero, hereda de la clase cliente
	 */
	public Viajero(String nombre, int id) {
		super(nombre, id);
		// TODO Auto-generated constructor stub
	}

	/*
	 * M�todo de cobro sobre escrito en el cual se cobra s�lo el 95% de la tarifa
	 */
	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		return this.getPrecio() * 0.95;
	}
}
