package deloitte.academy.lesson01.logic;

import java.util.ArrayList;

import deloitte.academy.lesson01.entity.Cliente;
import deloitte.academy.lesson01.entity.Habitacion;

public class Logic {
	public static ArrayList<Habitacion> habitaciones = new ArrayList<Habitacion>();

	/**
	 * M�tod para hacer el check in del cliente y verificando si est� o no
	 * disponible la habitaci�n para ser utilizada
	 * 
	 * @param numero  asociado con la habitaci�n para buscarla
	 * @param huesped cliente que ser� asociado a la habitaci�n
	 */
	public static void checkIn(int numero, Cliente huesped) {
		boolean found = false;

		for (Habitacion habitacion : Habitacion.habitaciones) {
			if (habitacion.getNumero() == numero && habitacion.getStatus()) {
				found = true;
				habitacion.setStatus(false);
				habitacion.setCliente(huesped);
				System.out.println("Check in realizado con �xito...");
				break;
			}
		}

		if (!found) {
			System.out.println("Check in incorrecto...");
		}
	}

	/**
	 * M�todo para realizar el checkout del cliente basado en el n�mero de
	 * habitaci�n y el importe que se hace para esa habitaci�n
	 * 
	 * @param numero de la habitaci�n para que pase a estar disponible
	 * @param importe de la habitaci�n para el usuario, es decir lo que tiene que pagar
	 */
	public static void checkOut(int numero, double importe) {
		boolean found = false;

		for (Habitacion habitacion : Habitacion.habitaciones) {
			if (!habitacion.getStatus()) {
				if (habitacion.getNumero() == numero) {
					found = true;
					habitacion.setStatus(true);
					habitacion.getCliente().setPrecio(importe);
					System.out.println("\nCheck out correcto del cliente " + habitacion.getCliente().getNombre()
							+ ":\nMonto a pagar $" + habitacion.getCliente().cobrar());
				}
			}
		}
	}

	/**
	 * M�todo para imprimir las habitaciones disponibles en el hotel, no es
	 * necesario ning�n par�metro
	 */
	public static void imprimirDisponibles() {
		for (Habitacion h : Logic.habitaciones) {
			if (h.getStatus()) {
				System.out.println("Habitaci�n " + h.getNumero() + " disponible...");
			}
		}
	}
}
