package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.entity.Empleado;
import deloitte.academy.lesson01.entity.Habitacion;
import deloitte.academy.lesson01.entity.Habitaciones;
import deloitte.academy.lesson01.entity.Normal;
import deloitte.academy.lesson01.entity.Viajero;
import deloitte.academy.lesson01.logic.Logic;

public class Main {

	public static void main(String[] args) {
		Normal normie = new Normal("Anakin", 23);
		Viajero viajero = new Viajero("Obi Wan", 23);
		Empleado empleado = new Empleado("Yoda", 23);
		
		new Habitacion(1);
		new Habitacion(2);
		new Habitacion(3);
		
		Logic.checkIn(1, normie);
		Logic.checkIn(2, viajero);
		Logic.checkIn(3, empleado);
		
		Logic.checkOut(1, Habitaciones.PRO.getCosto());
		Logic.checkOut(2, Habitaciones.BASIC.getCosto());
		Logic.checkOut(3, Habitaciones.PREMIUM.getCosto());
		
		Logic.imprimirDisponibles();
	}

}
